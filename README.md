# Excel操作类
实现对Excel的操作，需要程序集扩展：Microsoft Excel 对象库。

## 常见问题
##### 1.命名空间“Microsoft”中不存在类型或命名空间名“Office”
您未引用 Microsoft.Office.Interop.Excel.dll
##### 2.找不到 Microsoft.Office.Interop.Excel.dll
您未安装 Office 或它的 .Net可编程支持，请尝试安装。    
如果仍然找不到该文件，您可以访问[附件](https://gitee.com/fyy99/csharp-Excel/attach_files)下载

## 开发信息
C#处女作，还没有非常适应，望大佬指正。
#### 说明
目前支持的操作有：基本文件操作（打开关闭保存）、填写单元格、读取单元格
#### 参与开发
这是一个开源项目，欢迎您参与开发。    
您可以通过 [Issue](https://gitee.com/fyy99/csharp-Excel/issues) 指出问题、通过 [Pull Request](https://gitee.com/fyy99/csharp-Excel/pulls) 提交代码。
